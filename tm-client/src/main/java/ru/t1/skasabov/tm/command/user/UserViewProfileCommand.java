package ru.t1.skasabov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.UserProfileRequest;
import ru.t1.skasabov.tm.dto.response.UserProfileResponse;

@NoArgsConstructor
public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull private static final String NAME = "view-user-profile";

    @NotNull private static final String DESCRIPTION = "View profile of current user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER VIEW PROFILE]");
        @NotNull final UserProfileRequest request = new UserProfileRequest();
        request.setToken(getToken());
        @NotNull final UserProfileResponse response = getAuthEndpoint().profile(request);
        showUser(response.getUser());
    }

}
