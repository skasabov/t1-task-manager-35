package ru.t1.skasabov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.UserUpdateProfileRequest;
import ru.t1.skasabov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull private static final String NAME = "update-user-profile";

    @NotNull private static final String DESCRIPTION = "Update profile of current user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(
                lastName, firstName, middleName
        );
        request.setToken(getToken());
        getUserEndpoint().updateUserProfile(request);
    }

}
