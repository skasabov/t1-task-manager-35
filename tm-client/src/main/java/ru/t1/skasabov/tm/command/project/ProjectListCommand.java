package ru.t1.skasabov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.request.ProjectListRequest;
import ru.t1.skasabov.tm.dto.response.ProjectListResponse;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.util.TerminalUtil;

import java.util.Arrays;

@NoArgsConstructor
public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull private static final String NAME = "project-list";

    @NotNull private static final String DESCRIPTION = "Show all projects.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(sort);
        request.setToken(getToken());
        @NotNull final ProjectListResponse response = getProjectEndpoint().listProjects(request);
        renderProjects(response.getProjects());
    }

}
