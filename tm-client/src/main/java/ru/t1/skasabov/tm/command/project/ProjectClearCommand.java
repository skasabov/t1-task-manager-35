package ru.t1.skasabov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.ProjectClearRequest;

@NoArgsConstructor
public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull private static final String NAME = "project-clear";

    @NotNull private static final String DESCRIPTION = "Remove all projects.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest();
        request.setToken(getToken());
        getProjectEndpoint().clearProjects(request);
    }

}
