package ru.t1.skasabov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class FileScanner {

    @NotNull private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull private final Bootstrap bootstrap;

    @NotNull private final List<String> commands = new ArrayList<>();

    @Nullable private final File folder = new File("./");

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        @NotNull final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommandsWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        es.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    private void process() {
        if (folder == null) return;
        @NotNull final File[] files = folder.listFiles();
        if (files == null) return;
        for (@NotNull final File file : files) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    bootstrap.processCommand(fileName);
                }
                catch (@NotNull final Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

}
