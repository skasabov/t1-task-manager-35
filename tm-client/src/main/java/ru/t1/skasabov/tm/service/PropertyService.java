package ru.t1.skasabov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.service.IPropertyService;

@NoArgsConstructor
public final class PropertyService implements IPropertyService {

    @NotNull private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

}
