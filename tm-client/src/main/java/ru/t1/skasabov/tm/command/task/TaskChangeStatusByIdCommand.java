package ru.t1.skasabov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.util.TerminalUtil;

import java.util.Arrays;

@NoArgsConstructor
public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @NotNull private static final String NAME = "task-change-status-by-id";

    @NotNull private static final String DESCRIPTION = "Change task status by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(statusValue);
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(id, status);
        request.setToken(getToken());
        getTaskEndpoint().changeTaskStatusById(request);
    }

}
