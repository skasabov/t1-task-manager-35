package ru.t1.skasabov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.TaskCompleteByIndexRequest;
import ru.t1.skasabov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @NotNull private static final String NAME = "task-complete-by-index";

    @NotNull private static final String DESCRIPTION = "Complete task by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(index);
        request.setToken(getToken());
        getTaskEndpoint().completeTaskByIndex(request);
    }

}
