package ru.t1.skasabov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class ApplicationAboutClientCommand extends AbstractSystemCommand {

    @NotNull private static final String NAME = "about-client";

    @NotNull private static final String DESCRIPTION = "Show about client program.";

    @NotNull private static final String ARGUMENT = "-ac";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: " + getPropertyService().getAuthorName());
        System.out.println("email: " + getPropertyService().getAuthorEmail());
    }

}
