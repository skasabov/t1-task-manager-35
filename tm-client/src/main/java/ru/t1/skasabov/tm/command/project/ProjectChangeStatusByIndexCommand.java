package ru.t1.skasabov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.ProjectChangeStatusByIndexRequest;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.util.TerminalUtil;

import java.util.Arrays;

@NoArgsConstructor
public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @NotNull private static final String NAME = "project-change-status-by-index";

    @NotNull private static final String DESCRIPTION = "Change project status by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(index, status);
        request.setToken(getToken());
        getProjectEndpoint().changeProjectStatusByIndex(request);
    }

}
