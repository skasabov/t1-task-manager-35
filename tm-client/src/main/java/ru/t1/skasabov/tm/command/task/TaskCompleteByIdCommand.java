package ru.t1.skasabov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.TaskCompleteByIdRequest;
import ru.t1.skasabov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull private static final String NAME = "task-complete-by-id";

    @NotNull private static final String DESCRIPTION = "Complete task by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(id);
        request.setToken(getToken());
        getTaskEndpoint().completeTaskById(request);
    }

}
