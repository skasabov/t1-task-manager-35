package ru.t1.skasabov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.TaskGetByProjectIdRequest;
import ru.t1.skasabov.tm.dto.response.TaskGetByProjectIdResponse;
import ru.t1.skasabov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull private static final String NAME = "task-show-by-project-id";

    @NotNull private static final String DESCRIPTION = "Show task list by project id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskGetByProjectIdRequest request = new TaskGetByProjectIdRequest(projectId);
        request.setToken(getToken());
        @NotNull final TaskGetByProjectIdResponse response = getTaskEndpoint().findAllByProjectId(request);
        renderTasks(response.getTasks());
    }

}
