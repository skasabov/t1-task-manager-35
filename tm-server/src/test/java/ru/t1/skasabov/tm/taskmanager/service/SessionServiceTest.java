package ru.t1.skasabov.tm.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.skasabov.tm.api.repository.ISessionRepository;
import ru.t1.skasabov.tm.api.service.ISessionService;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.model.Session;
import ru.t1.skasabov.tm.repository.SessionRepository;
import ru.t1.skasabov.tm.service.SessionService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SessionServiceTest {
    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_ONE = UUID.randomUUID().toString();

    private static final String USER_ID_TWO = UUID.randomUUID().toString();

    @NotNull private List<Session> sessionList;

    @NotNull private ISessionService sessionService;

    @Before
    public void initTest() {
        sessionList = new ArrayList<>();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository();
        sessionService = new SessionService(sessionRepository);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i <= 5) session.setUserId(USER_ID_ONE);
            else session.setUserId(USER_ID_TWO);
            sessionRepository.add(session);
            sessionList.add(session);
        }
    }

    @Test
    public void testAdd() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionService.getSize());
        sessionService.add(new Session());
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, sessionService.getSize());
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddNull() {
        sessionService.add(null);
    }

    @Test
    public void testAddForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, sessionService.getSize(USER_ID_ONE));
        Session session = new Session();
        session.setUserId(USER_ID_ONE);
        sessionService.add(session);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, sessionService.getSize(USER_ID_ONE));
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddNullForUser() {
        sessionService.add(USER_ID_ONE, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testAddForEmptyUser() {
        Session session = new Session();
        session.setUserId(USER_ID_ONE);
        sessionService.add("", session);
    }

    @Test
    public void testAddAll() {
        @NotNull final List<Session> actualProjects = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            actualProjects.add(new Session());
        }
        sessionService.addAll(actualProjects);
        Assert.assertEquals(NUMBER_OF_ENTRIES + 4, sessionService.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<Session> actualProjects = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            actualProjects.add(new Session());
        }
        sessionService.set(actualProjects);
        Assert.assertEquals(10, sessionService.getSize());
    }

    @Test
    public void testClearAll() {
        sessionService.removeAll();
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testClearAllForUser() {
        sessionService.removeAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, sessionService.getSize());
    }

    @Test
    public void testClear() {
        sessionService.removeAll(sessionList.subList(0, 5));
        Assert.assertEquals(5, sessionService.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Session> sessionList = sessionService.findAll();
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionList.size());
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Session> sessionList = sessionService.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, sessionList.size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllForEmptyUser() {
        sessionService.findAll("");
    }

    @Test
    public void testFindById() {
        @NotNull final String sessionId = sessionService.findAll().get(0).getId();
        Assert.assertEquals(sessionList.get(0), sessionService.findOneById(sessionId));
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyId() {
        Assert.assertNull(sessionService.findOneById(""));
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final String sessionId = sessionService.findAll().get(0).getId();
        Assert.assertEquals(sessionList.get(0), sessionService.findOneById(USER_ID_ONE, sessionId));
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyIdForUser() {
        Assert.assertNull(sessionService.findOneById(USER_ID_ONE, ""));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIdForEmptyUser() {
        @NotNull final String sessionId = sessionService.findAll().get(0).getId();
        sessionService.findOneById("", sessionId);
    }

    @Test
    public void testFindByIdSessionNotFound() {
        Assert.assertNull(sessionService.findOneById("some id"));
    }

    @Test
    public void testFindByIdSessionNotFoundForUser() {
        String sessionId = sessionList.get(7).getId();
        Assert.assertNull(sessionService.findOneById(USER_ID_ONE, sessionId));
    }

    @Test
    public void testFindByIndex() {
        Assert.assertEquals(sessionList.get(0), sessionService.findOneByIndex(0));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndex() {
        sessionService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndex() {
        sessionService.findOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndex() {
        sessionService.findOneByIndex(12);
    }

    @Test
    public void testFindByIndexForUser() {
        Assert.assertEquals(sessionList.get(0), sessionService.findOneByIndex(USER_ID_ONE, 0));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndexForUser() {
        sessionService.findOneByIndex(USER_ID_ONE, null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndexForUser() {
        sessionService.findOneByIndex(USER_ID_ONE, -2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndexForUser() {
        sessionService.findOneByIndex(USER_ID_ONE, 12);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIndexForEmptyUser() {
        sessionService.findOneByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexSessionNotFound() {
        sessionService.findOneByIndex(USER_ID_ONE, 7);
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionService.getSize());
        sessionService.add(new Session());
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, sessionService.getSize());
        sessionService.removeAll();
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, sessionService.getSize(USER_ID_ONE));
        Session session = new Session();
        session.setUserId(USER_ID_ONE);
        sessionService.add(session);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, sessionService.getSize(USER_ID_ONE));
        sessionService.removeAll(USER_ID_ONE);
        Assert.assertEquals(0, sessionService.getSize(USER_ID_ONE));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testGetSizeForEmptyUser() {
        sessionService.getSize("");
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = sessionList.get(0).getId();
        @NotNull final String invalidId = "some id";
        Assert.assertFalse(sessionService.existsById(invalidId));
        Assert.assertTrue(sessionService.existsById(validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyId() {
        sessionService.existsById("");
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = sessionList.get(0).getId();
        @NotNull final String invalidId = sessionList.get(7).getId();
        Assert.assertFalse(sessionService.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(sessionService.existsById(USER_ID_ONE, validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyIdForUser() {
        sessionService.existsById(USER_ID_ONE, "");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testIsNotFoundForEmptyUser() {
        sessionService.existsById("", sessionList.get(0).getId());
    }

    @Test
    public void testRemove() {
        @NotNull final Session session = sessionService.findAll().get(3);
        sessionService.removeOne(session);
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionService.getSize());
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveNull() {
        sessionService.removeOne(null);
    }

    @Test
    public void testRemoveForUser() {
        @NotNull final Session session = sessionService.findAll().get(3);
        sessionService.removeOne(USER_ID_ONE, session);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 - 1, sessionService.getSize(USER_ID_ONE));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveForEmptyUser() {
        @NotNull final Session session = sessionService.findAll().get(3);
        sessionService.removeOne("", session);
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveNullForUser() {
        sessionService.removeOne(USER_ID_ONE, null);
    }

    @Test
    public void testRemoveById() {
        @NotNull final String sessionId = sessionService.findAll().get(3).getId();
        sessionService.removeOneById(sessionId);
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionService.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        @NotNull final String sessionId = sessionService.findAll().get(3).getId();
        sessionService.removeOneById(USER_ID_ONE, sessionId);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 - 1, sessionService.getSize(USER_ID_ONE));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyId() {
        sessionService.removeOneById("");
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyIdForUser() {
        sessionService.removeOneById(USER_ID_ONE, "");
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveByIdSessionNotFound() {
        sessionService.removeOneById("some id");
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveByIdSessionNotFoundForUser() {
        String sessionId = sessionList.get(7).getId();
        sessionService.removeOneById(USER_ID_ONE, sessionId);
    }

    @Test
    public void testRemoveByIndex() {
        Assert.assertEquals(sessionList.get(0), sessionService.removeOneByIndex(0));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByEmptyIndex() {
        sessionService.removeOneByIndex(null);
    }

    @Test
    public void testRemoveByIndexForUser() {
        Assert.assertEquals(sessionList.get(0), sessionService.removeOneByIndex(USER_ID_ONE, 0));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByEmptyIndexForUser() {
        sessionService.removeOneByIndex(USER_ID_ONE, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveByIndexForEmptyUser() {
        sessionService.removeOneByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexSessionNotFound() {
        sessionService.removeOneByIndex(USER_ID_ONE, 7);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndex() {
        sessionService.removeOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndexForUser() {
        sessionService.removeOneByIndex(USER_ID_ONE, -2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndex() {
        sessionService.removeOneByIndex(12);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndexForUser() {
        sessionService.removeOneByIndex(USER_ID_ONE, 12);
    }

    @After
    public void clearRepository() {
        sessionList.clear();
        sessionService.removeAll();
    }
    
}
