package ru.t1.skasabov.tm.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.skasabov.tm.api.repository.IProjectRepository;
import ru.t1.skasabov.tm.api.repository.ITaskRepository;
import ru.t1.skasabov.tm.api.repository.IUserRepository;
import ru.t1.skasabov.tm.api.service.IPropertyService;
import ru.t1.skasabov.tm.api.service.IUserService;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.entity.UserNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.exception.user.ExistsEmailException;
import ru.t1.skasabov.tm.exception.user.ExistsLoginException;
import ru.t1.skasabov.tm.exception.user.RoleEmptyException;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.repository.ProjectRepository;
import ru.t1.skasabov.tm.repository.TaskRepository;
import ru.t1.skasabov.tm.repository.UserRepository;
import ru.t1.skasabov.tm.service.PropertyService;
import ru.t1.skasabov.tm.service.UserService;
import ru.t1.skasabov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest {

    @NotNull private List<User> userList;

    @NotNull private IUserService userService;

    @NotNull private IPropertyService propertyService;

    @Before
    public void initTest() {
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        propertyService = new PropertyService();
        userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);
        @NotNull final User admin = new User();
        admin.setLogin("admin");
        admin.setPasswordHash(HashUtil.salt(propertyService, "admin"));
        admin.setEmail("admin@admin");
        @NotNull final User user = new User();
        user.setLogin("user");
        user.setPasswordHash(HashUtil.salt(propertyService, "user"));
        user.setEmail("user@user");
        @NotNull final User cat = new User();
        cat.setLogin("cat");
        cat.setPasswordHash(HashUtil.salt(propertyService, "cat"));
        cat.setEmail("cat@cat");
        @NotNull final User mouse = new User();
        mouse.setLogin("mouse");
        mouse.setPasswordHash(HashUtil.salt(propertyService, "mouse"));
        mouse.setEmail("mouse@mouse");
        userList = new ArrayList<>();
        userList.add(admin);
        userList.add(user);
        userList.add(cat);
        userList.add(mouse);
        userService.add(admin);
        userService.add(user);
        userService.add(cat);
        userService.add(mouse);
        userService.lockUserByLogin("mouse");
    }

    @Test
    public void testAdd() {
        Assert.assertEquals(4, userService.getSize());
        userService.add(new User());
        Assert.assertEquals(5, userService.getSize());
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddNull() {
        userService.add(null);
    }

    @Test
    public void testAddAll() {
        @NotNull final List<User> actualProjects = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            actualProjects.add(new User());
        }
        userService.addAll(actualProjects);
        Assert.assertEquals(8, userService.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<User> actualProjects = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            actualProjects.add(new User());
        }
        userService.set(actualProjects);
        Assert.assertEquals(10, userService.getSize());
    }

    @Test
    public void testCreate() {
        Assert.assertEquals(4, userService.getSize());
        userService.create("dog", "dog", "dog@dog");
        Assert.assertEquals(5, userService.getSize());
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreateEmptyLogin() {
        userService.create("", "dog", "dog@dog");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreateEmptyPassword() {
        userService.create("dog", "", "dog@dog");
    }

    @Test(expected = EmailEmptyException.class)
    public void testCreateEmptyEmail() {
        userService.create("dog", "dog", "");
    }

    @Test(expected = ExistsLoginException.class)
    public void testCreateLoginExists() {
        userService.create("cat", "cat", "cat@cat");
    }

    @Test(expected = ExistsEmailException.class)
    public void testCreateEmailExists() {
        userService.create("dog", "dog", "cat@cat");
    }

    @Test
    public void testCreateRole() {
        Assert.assertEquals(4, userService.getSize());
        userService.create("dog", "dog", Role.USUAL);
        Assert.assertEquals(5, userService.getSize());
    }

    @Test(expected = RoleEmptyException.class)
    public void testCreateRoleNull() {
        userService.create("dog", "dog", (Role) null);
    }

    @Test
    public void testClearAll() {
        userService.removeAll();
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testClear() {
        userService.removeAll(userList.subList(0, 2));
        Assert.assertEquals(2, userService.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<User> userList = userService.findAll();
        Assert.assertEquals(4, userList.size());
    }

    @Test
    public void testFindById() {
        @NotNull final String userId = userService.findAll().get(0).getId();
        Assert.assertEquals(userList.get(0), userService.findOneById(userId));
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyId() {
        Assert.assertNull(userService.findOneById(""));
    }

    @Test
    public void testFindByIdUserNotFound() {
        Assert.assertNull(userService.findOneById("some id"));
    }

    @Test
    public void testFindByLogin() {
        Assert.assertEquals(userList.get(0), userService.findByLogin("admin"));
    }

    @Test(expected = LoginEmptyException.class)
    public void testFindByEmptyLogin() {
        Assert.assertNull(userService.findByLogin(""));
    }

    @Test
    public void testFindByLoginUserNotFound() {
        Assert.assertNull(userService.findByLogin("dog"));
    }

    @Test
    public void testFindByEmail() {
        Assert.assertEquals(userList.get(0), userService.findByEmail("admin@admin"));
    }

    @Test(expected = EmailEmptyException.class)
    public void testFindByEmptyEmail() {
        Assert.assertNull(userService.findByEmail(""));
    }

    @Test
    public void testFindByEmailUserNotFound() {
        Assert.assertNull(userService.findByEmail("dog@dog"));
    }

    @Test
    public void testFindByIndex() {
        Assert.assertEquals(userList.get(0), userService.findOneByIndex(0));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndex() {
        userService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndex() {
        userService.findOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndex() {
        userService.findOneByIndex(5);
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(4, userService.getSize());
        userService.add(new User());
        Assert.assertEquals(5, userService.getSize());
        userService.removeAll();
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = userList.get(0).getId();
        @NotNull final String invalidId = "some id";
        Assert.assertFalse(userService.existsById(invalidId));
        Assert.assertTrue(userService.existsById(validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyId() {
        userService.existsById("");
    }

    @Test
    public void testRemove() {
        @NotNull final User user = userService.findAll().get(3);
        userService.removeOne(user);
        Assert.assertEquals(3, userService.getSize());
    }

    @Test
    public void testRemoveNull() {
        userService.removeOne(null);
        Assert.assertEquals(4, userService.getSize());
    }

    @Test
    public void testRemoveById() {
        @NotNull final String userId = userService.findAll().get(3).getId();
        userService.removeOneById(userId);
        Assert.assertEquals(3, userService.getSize());
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyId() {
        Assert.assertNull(userService.removeOneById(""));
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveByIdUserNotFound() {
        userService.removeOneById("some id");
    }

    @Test
    public void testRemoveByIndex() {
        Assert.assertEquals(userList.get(0), userService.removeOneByIndex(0));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndex() {
        userService.removeOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndex() {
        userService.removeOneByIndex(5);
    }

    @Test
    public void testRemoveByLogin() {
        userService.removeByLogin("mouse");
        Assert.assertEquals(3, userService.getSize());
    }

    @Test(expected = LoginEmptyException.class)
    public void testRemoveByEmptyLogin() {
        Assert.assertNull(userService.removeByLogin(""));
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveByLoginUserNotFound() {
        Assert.assertNull(userService.removeByLogin("dog"));
    }

    @Test
    public void testRemoveByEmail() {
        userService.removeByEmail("mouse@mouse");
        Assert.assertEquals(3, userService.getSize());
    }

    @Test(expected = EmailEmptyException.class)
    public void testRemoveByEmptyEmail() {
        Assert.assertNull(userService.removeByEmail(""));
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveByEmailUserNotFound() {
        Assert.assertNull(userService.removeByEmail("dog@dog"));
    }

    @Test
    public void testSetPassword() {
        @NotNull final String userId = userList.get(0).getId();
        @Nullable final String passwordHash = userList.get(0).getPasswordHash();
        userService.setPassword(userId, "admin_admin");
        Assert.assertNotEquals(HashUtil.salt(propertyService, "admin_admin"), passwordHash);
    }

    @Test(expected = IdEmptyException.class)
    public void testSetPasswordEmptyId() {
        userService.setPassword("", "admin");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testSetEmptyPassword() {
        @NotNull final String userId = userList.get(0).getId();
        userService.setPassword(userId, "");
    }

    @Test(expected = UserNotFoundException.class)
    public void testSetPasswordUserNotFound() {
        userService.setPassword("some_id", "admin");
    }

    @Test
    public void testUpdateUser() {
        @NotNull final String userId = userList.get(0).getId();
        @Nullable final User user = userService.updateUser(userId, "admin", "admin", "admin");
        Assert.assertEquals(userList.get(0), user);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateUserEmptyId() {
        userService.updateUser("", "admin", "admin", "admin");
    }

    @Test(expected = UserNotFoundException.class)
    public void testUpdateUserNotFound() {
        userService.updateUser("some_id", "admin", "admin", "admin");
    }

    @Test
    public void testLockUserByLogin() {
        @Nullable final User user = userService.lockUserByLogin("cat");
        Assert.assertTrue(user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void testLockUserByEmptyLogin() {
        userService.lockUserByLogin("");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLockUserByLoginNotFound() {
        userService.lockUserByLogin("dog");
    }

    @Test
    public void testUnlockUserByLogin() {
        @Nullable final User user = userService.unlockUserByLogin("cat");
        Assert.assertFalse(user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void testUnlockUserByEmptyLogin() {
        userService.unlockUserByLogin("");
    }

    @Test(expected = UserNotFoundException.class)
    public void testUnlockUserByLoginNotFound() {
        userService.unlockUserByLogin("dog");
    }

    @Test
    public void testLoginExist() {
        Assert.assertTrue(userService.isLoginExist("cat"));
    }

    @Test
    public void testEmptyLoginExist() {
        Assert.assertFalse(userService.isLoginExist(""));
    }

    @Test
    public void testEmailExist() {
        Assert.assertTrue(userService.isEmailExist("cat@cat"));
    }

    @Test
    public void testEmptyEmailExist() {
        Assert.assertFalse(userService.isEmailExist(""));
    }

    @After
    public void clearRepository() {
        userList.clear();
        userService.removeAll();
    }

}
