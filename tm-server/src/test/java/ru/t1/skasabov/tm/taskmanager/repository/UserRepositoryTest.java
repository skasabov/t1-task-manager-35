package ru.t1.skasabov.tm.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.skasabov.tm.api.repository.IUserRepository;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull private List<User> userList;

    @NotNull private IUserRepository userRepository;

    @Before
    public void initRepository() {
        userList = new ArrayList<>();
        userRepository = new UserRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("user " + i);
            user.setEmail("user@" + i);
            user.setRole(Role.USUAL);
            userRepository.add(user);
            userList.add(user);
        }
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        userRepository.add(new User());
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testAddAll() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<User> userList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            userList.add(new User());
        }
        userRepository.addAll(userList);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testAddNull() {
        userRepository.addAll(null);
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<User> userList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            userList.add(new User());
        }
        userRepository.set(userList);
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    public void testSetNull() {
        final int expectedNumberOfEntries = 0;
        userRepository.set(null);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        userRepository.removeAll();
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testClearAll() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES / 2;
        @NotNull final List<User> actualUserList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES / 2; i++) {
            actualUserList.add(userList.get(i));
        }
        userRepository.removeAll(actualUserList);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<User> actualUserList = userRepository.findAll();
        Assert.assertEquals(userList, actualUserList);
    }

    @Test
    public void testFindByIdPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findOneById(user.getId()));
            Assert.assertEquals(user, userRepository.findOneById(user.getId()));
        }
    }

    @Test
    public void testFindByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(userRepository.findOneById(id));
    }

    @Test
    public void testFindByEmailPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findByEmail(user.getEmail()));
            Assert.assertEquals(user, userRepository.findByEmail(user.getEmail()));
        }
    }

    @Test
    public void testFindByEmailNegative() {
        @NotNull final String invalidEmail = "some email";
        Assert.assertNull(userRepository.findByEmail(invalidEmail));
    }

    @Test
    public void testFindByLoginPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findByLogin(user.getLogin()));
            Assert.assertEquals(user, userRepository.findByLogin(user.getLogin()));
        }
    }

    @Test
    public void testFindByLoginNegative() {
        @NotNull final String invalidLogin = "some login";
        Assert.assertNull(userRepository.findByLogin(invalidLogin));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final User expectedOne = userList.get(0);
        @NotNull final User expectedTwo = userList.get(userRepository.getSize() - 1);
        @NotNull final User expectedThree = userList.get(userRepository.getSize() / 2);
        Assert.assertEquals(expectedOne, userRepository.findOneByIndex(0));
        Assert.assertEquals(expectedTwo, userRepository.findOneByIndex(userRepository.getSize() - 1));
        Assert.assertEquals(expectedThree, userRepository.findOneByIndex(userRepository.getSize() / 2));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    public void testIsNotFoundByEmailPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertTrue(userRepository.isEmailExist(user.getEmail()));
        }
    }

    @Test
    public void testIsNotFoundByEmailNegative() {
        Assert.assertFalse(userRepository.isEmailExist("some email"));
    }

    @Test
    public void testIsNotFoundByLoginPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertTrue(userRepository.isLoginExist(user.getLogin()));
        }
    }

    @Test
    public void testIsNotFoundByLoginNegative() {
        Assert.assertFalse(userRepository.isLoginExist("some login"));
    }

    @Test
    public void testRemovePositive() {
        for (@NotNull final User user : userList) {
            userRepository.removeOne(user);
            Assert.assertNull(userRepository.findOneById(user.getId()));
        }
    }

    @Test
    public void testRemoveNegative() {
        @NotNull final User userNotFromRepository = new User();
        userRepository.removeOne(userNotFromRepository);
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    public void testRemoveByIdPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.removeOneById(user.getId()));
            Assert.assertNull(userRepository.findOneById(user.getId()));
        }
    }

    @Test
    public void testRemoveByIdNegative() {
        @NotNull final String idNotFromRepository = UUID.randomUUID().toString();
        Assert.assertThrows(ModelEmptyException.class, () -> userRepository.removeOneById(idNotFromRepository));
    }

    @Test
    public void testRemoveByIndex() {
        @NotNull final User expectedOne = userList.get(0);
        @NotNull final User expectedTwo = userList.get(userRepository.getSize() - 1);
        @NotNull final User expectedThree = userList.get(userRepository.getSize() / 2);
        Assert.assertEquals(expectedOne, userRepository.removeOneByIndex(0));
        Assert.assertEquals(expectedTwo, userRepository.removeOneByIndex(userRepository.getSize() - 1));
        Assert.assertEquals(expectedThree, userRepository.removeOneByIndex(userRepository.getSize() / 2));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 3, userRepository.getSize());
    }

    @After
    public void clearRepository() {
        userList.clear();
        userRepository.removeAll();
    }

}
