package ru.t1.skasabov.tm.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.skasabov.tm.api.repository.ITaskRepository;
import ru.t1.skasabov.tm.api.service.ITaskService;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.enumerated.TaskSort;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.entity.TaskNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.repository.TaskRepository;
import ru.t1.skasabov.tm.service.TaskService;

import java.util.*;

public class TaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_ONE = UUID.randomUUID().toString();

    private static final String USER_ID_TWO = UUID.randomUUID().toString();

    @NotNull private final Project projectOne = new Project();

    @NotNull private final Project projectTwo = new Project();

    @NotNull private List<Task> taskList;

    @NotNull private ITaskService taskService;

    @Before
    public void initTest() {
        taskList = new ArrayList<>();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        taskService = new TaskService(taskRepository);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            if (i <= 5) {
                task.setUserId(USER_ID_ONE);
                task.setProjectId(projectOne.getId());
            }
            else {
                task.setUserId(USER_ID_TWO);
                task.setProjectId(projectTwo.getId());
            }
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateForEmptyUser() {
        taskService.create("", "test", "");
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateEmptyName() {
        @NotNull final String userId = UUID.randomUUID().toString();
        taskService.create(userId, "", "");
    }

    @Test
    public void testCreateName() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Task";
        taskService.create(userId, name, "");
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
        @Nullable final Task actualTask = taskService.findOneByIndex(taskService.getSize() - 1);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(userId, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
    }

    @Test
    public void createTask() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test task";
        @NotNull final String description = "Test Description";
        @NotNull final Date dateBegin = new Date();
        @NotNull final Date dateEnd = new Date();
        taskService.create(userId, name, description, dateBegin, dateEnd);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
        @Nullable final Task actualTask = taskService.findOneByIndex(taskService.getSize() - 1);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(userId, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
        Assert.assertEquals(dateBegin, actualTask.getDateBegin());
        Assert.assertEquals(dateEnd, actualTask.getDateEnd());
    }

    @Test
    public void testUpdateById() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @Nullable final String id = taskService.findAll().get(0).getId();
        @NotNull final String name = "Test task One";
        @NotNull final String description = "Test Description One";
        @NotNull final Task task = taskService.updateById(userId, id, name, description);
        Assert.assertNotNull(task);
        Assert.assertEquals(userId, task.getUserId());
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByEmptyId() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @NotNull final String name = "Test task One";
        @NotNull final String description = "Test Description One";
        taskService.updateById(userId, "", name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIdForEmptyUser() {
        @Nullable final String id = taskService.findAll().get(0).getId();
        @NotNull final String name = "Test task One";
        @NotNull final String description = "Test Description One";
        taskService.updateById("", id, name, description);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdWithEmptyName() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @Nullable final String id = taskService.findAll().get(0).getId();
        @NotNull final String description = "Test Description One";
        taskService.updateById(userId, id, "", description);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUpdateByIdTaskNotFound() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @Nullable final String id = taskService.findAll().get(7).getId();
        @NotNull final String name = "Test task One";
        @NotNull final String description = "Test Description One";
        taskService.updateById(userId, id, name, description);
    }

    @Test
    public void testUpdateByIndex() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @NotNull final String name = "Test task One";
        @NotNull final String description = "Test Description One";
        @NotNull final Task task = taskService.updateByIndex(userId, 0, name, description);
        Assert.assertNotNull(task);
        Assert.assertEquals(userId, task.getUserId());
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByEmptyIndex() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @NotNull final String name = "Test task One";
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex(userId, null, name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndexForEmptyUser() {
        @NotNull final String name = "Test task One";
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex("", 0, name, description);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndexWithEmptyName() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex(userId, 0, "", description);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByNegativeIndex() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @NotNull final String name = "Test task One";
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex(userId, -2, name, description);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIncorrectIndex() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @NotNull final String name = "Test task One";
        @NotNull final String description = "Test Description One";
        taskService.updateByIndex(userId, 7, name, description);
    }

    @Test
    public void testChangeTaskStatusById() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @Nullable final String id = taskService.findAll().get(0).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        @NotNull final Task task = taskService.changeTaskStatusById(userId, id, status);
        Assert.assertNotNull(task);
        Assert.assertEquals(userId, task.getUserId());
        Assert.assertEquals(status, task.getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeTaskStatusByEmptyId() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusById(userId, "", status);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeTaskStatusByIdForEmptyUser() {
        @Nullable final String id = taskService.findAll().get(0).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusById("", id, status);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeTaskStatusByIdWithEmptyStatus() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @Nullable final String id = taskService.findAll().get(0).getId();
        taskService.changeTaskStatusById(userId, id, Status.toStatus(null));
    }

    @Test(expected = StatusIncorrectException.class)
    public void testChangeTaskStatusByIdWithIncorrectStatus() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @Nullable final String id = taskService.findAll().get(0).getId();
        taskService.changeTaskStatusById(userId, id, Status.toStatus("123"));
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeTaskStatusByIdTaskNotFound() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @Nullable final String id = taskService.findAll().get(7).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusById(userId, id, status);
    }

    @Test
    public void testChangeTaskStatusByIndex() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @NotNull final Status status = Status.NOT_STARTED;
        @NotNull final Task task = taskService.changeTaskStatusByIndex(userId, 0, status);
        Assert.assertNotNull(task);
        Assert.assertEquals(userId, task.getUserId());
        Assert.assertEquals(status, task.getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeTaskStatusByEmptyIndex() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusByIndex(userId, null, status);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeTaskStatusByIndexForEmptyUser() {
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusByIndex("", 0, status);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeTaskStatusByIndexWithEmptyStatus() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        taskService.changeTaskStatusByIndex(userId, 0, Status.toStatus(null));
    }

    @Test(expected = StatusIncorrectException.class)
    public void testChangeTaskStatusByIndexWithIncorrectStatus() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        taskService.changeTaskStatusByIndex(userId, 0, Status.toStatus("123"));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeTaskStatusByIncorrectIndex() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusByIndex(userId, 7, status);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeTaskStatusByNegativeIndex() {
        @Nullable final String userId = taskService.findAll().get(0).getUserId();
        @NotNull final Status status = Status.NOT_STARTED;
        taskService.changeTaskStatusByIndex(userId, -2, status);
    }

    @Test
    public void testFindAllByProjectId() {
        @NotNull final List<Task> tasks = taskService.findAllByProjectId(USER_ID_ONE, projectOne.getId());
        Assert.assertEquals(taskList.subList(0, 5), tasks);
    }

    @Test
    public void testFindAllByIncorrectProjectId() {
        @NotNull final List<Task> tasks = taskService.findAllByProjectId(USER_ID_ONE, projectTwo.getId());
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllByProjectIdForEmptyUser() {
        taskService.findAllByProjectId("", projectOne.getId());
    }

    @Test
    public void testAdd() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
        taskService.add(new Task());
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, taskService.getSize());
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddNull() {
        taskService.add(null);
    }

    @Test
    public void testAddForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, taskService.getSize(USER_ID_ONE));
        Task task = new Task();
        task.setUserId(USER_ID_ONE);
        taskService.add(task);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, taskService.getSize(USER_ID_ONE));
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddNullForUser() {
        taskService.add(USER_ID_ONE, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testAddForEmptyUser() {
        Task task = new Task();
        task.setUserId(USER_ID_ONE);
        taskService.add("", task);
    }

    @Test
    public void testAddAll() {
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            actualTasks.add(new Task());
        }
        taskService.addAll(actualTasks);
        Assert.assertEquals(NUMBER_OF_ENTRIES + 4, taskService.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            actualTasks.add(new Task());
        }
        taskService.set(actualTasks);
        Assert.assertEquals(10, taskService.getSize());
    }

    @Test
    public void testClearAll() {
        taskService.removeAll();
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testClearAllForUser() {
        taskService.removeAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, taskService.getSize());
    }

    @Test
    public void testClear() {
        taskService.removeAll(taskList.subList(0, 5));
        Assert.assertEquals(5, taskService.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> taskList = taskService.findAll();
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskList.size());
    }

    @Test
    public void testFindAllWithComparator() {
        @NotNull final TaskSort sort = TaskSort.BY_NAME;
        @NotNull final List<Task> taskSortList = taskService.findAll(sort.getComparator());
        Assert.assertEquals(taskList, taskSortList);
    }

    @Test
    public void testFindAllWithEmptyComparator() {
        @NotNull final List<Task> taskSortList = taskService.findAll((Comparator<Task>) null);
        Assert.assertEquals(taskSortList, taskList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Task> taskList = taskService.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, taskList.size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllForEmptyUser() {
        taskService.findAll("");
    }

    @Test
    public void testFindAllWithComparatorForUser() {
        @NotNull final TaskSort sort = TaskSort.BY_NAME;
        @NotNull final List<Task> taskSortList = taskService.findAll(USER_ID_ONE, sort.getComparator());
        Assert.assertEquals(taskList.subList(0, 5), taskSortList);
    }

    @Test
    public void testFindAllWithEmptyComparatorForUser() {
        @NotNull final List<Task> taskSortList = taskService.findAll(USER_ID_ONE, null);
        Assert.assertEquals(taskList.subList(0, 5), taskSortList);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllWithComparatorForEmptyUser() {
        @NotNull final TaskSort sort = TaskSort.BY_NAME;
        taskService.findAll("", sort.getComparator());
    }

    @Test
    public void testFindById() {
        @NotNull final String taskId = taskService.findAll().get(0).getId();
        Assert.assertEquals(taskList.get(0), taskService.findOneById(taskId));
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyId() {
        Assert.assertNull(taskService.findOneById(""));
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final String taskId = taskService.findAll().get(0).getId();
        Assert.assertEquals(taskList.get(0), taskService.findOneById(USER_ID_ONE, taskId));
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyIdForUser() {
        Assert.assertNull(taskService.findOneById(USER_ID_ONE, ""));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIdForEmptyUser() {
        @NotNull final String taskId = taskService.findAll().get(0).getId();
        taskService.findOneById("", taskId);
    }

    @Test
    public void testFindByIdTaskNotFound() {
        Assert.assertNull(taskService.findOneById("some id"));
    }

    @Test
    public void testFindByIdTaskNotFoundForUser() {
        String taskId = taskList.get(7).getId();
        Assert.assertNull(taskService.findOneById(USER_ID_ONE, taskId));
    }

    @Test
    public void testFindByIndex() {
        Assert.assertEquals(taskList.get(0), taskService.findOneByIndex(0));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndex() {
        taskService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndex() {
        taskService.findOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndex() {
        taskService.findOneByIndex(12);
    }

    @Test
    public void testFindByIndexForUser() {
        Assert.assertEquals(taskList.get(0), taskService.findOneByIndex(USER_ID_ONE, 0));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndexForUser() {
        taskService.findOneByIndex(USER_ID_ONE, null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndexForUser() {
        taskService.findOneByIndex(USER_ID_ONE, -2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndexForUser() {
        taskService.findOneByIndex(USER_ID_ONE, 12);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIndexForEmptyUser() {
        taskService.findOneByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexTaskNotFound() {
        taskService.findOneByIndex(USER_ID_ONE, 7);
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
        taskService.add(new Task());
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, taskService.getSize());
        taskService.removeAll();
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, taskService.getSize(USER_ID_ONE));
        Task task = new Task();
        task.setUserId(USER_ID_ONE);
        taskService.add(task);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, taskService.getSize(USER_ID_ONE));
        taskService.removeAll(USER_ID_ONE);
        Assert.assertEquals(0, taskService.getSize(USER_ID_ONE));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testGetSizeForEmptyUser() {
        taskService.getSize("");
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = taskList.get(0).getId();
        @NotNull final String invalidId = "some id";
        Assert.assertFalse(taskService.existsById(invalidId));
        Assert.assertTrue(taskService.existsById(validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyId() {
        taskService.existsById("");
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = taskList.get(0).getId();
        @NotNull final String invalidId = taskList.get(7).getId();
        Assert.assertFalse(taskService.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(taskService.existsById(USER_ID_ONE, validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyIdForUser() {
        taskService.existsById(USER_ID_ONE, "");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testIsNotFoundForEmptyUser() {
        taskService.existsById("", taskList.get(0).getId());
    }

    @Test
    public void testRemove() {
        @NotNull final Task task = taskService.findAll().get(3);
        taskService.removeOne(task);
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, taskService.getSize());
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveNull() {
        taskService.removeOne(null);
    }

    @Test
    public void testRemoveForUser() {
        @NotNull final Task task = taskService.findAll().get(3);
        taskService.removeOne(USER_ID_ONE, task);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 - 1, taskService.getSize(USER_ID_ONE));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveForEmptyUser() {
        @NotNull final Task task = taskService.findAll().get(3);
        taskService.removeOne("", task);
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveNullForUser() {
        taskService.removeOne(USER_ID_ONE, null);
    }

    @Test
    public void testRemoveById() {
        @NotNull final String taskId = taskService.findAll().get(3).getId();
        taskService.removeOneById(taskId);
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, taskService.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        @NotNull final String taskId = taskService.findAll().get(3).getId();
        taskService.removeOneById(USER_ID_ONE, taskId);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 - 1, taskService.getSize(USER_ID_ONE));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyId() {
        Assert.assertNull(taskService.removeOneById(""));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyIdForUser() {
        taskService.removeOneById(USER_ID_ONE, "");
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveByIdTaskNotFound() {
        taskService.removeOneById("some id");
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveByIdTaskNotFoundForUser() {
        String taskId = taskList.get(7).getId();
        taskService.removeOneById(USER_ID_ONE, taskId);
    }

    @Test
    public void testRemoveByIndex() {
        Assert.assertEquals(taskList.get(0), taskService.removeOneByIndex(0));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByEmptyIndex() {
        taskService.removeOneByIndex(null);
    }

    @Test
    public void testRemoveByIndexForUser() {
        Assert.assertEquals(taskList.get(0), taskService.removeOneByIndex(USER_ID_ONE, 0));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByEmptyIndexForUser() {
        taskService.removeOneByIndex(USER_ID_ONE, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveByIndexForEmptyUser() {
        taskService.removeOneByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexTaskNotFound() {
        taskService.removeOneByIndex(USER_ID_ONE, 7);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndex() {
        taskService.removeOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndexForUser() {
        taskService.removeOneByIndex(USER_ID_ONE, -2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndex() {
        taskService.removeOneByIndex(12);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndexForUser() {
        taskService.removeOneByIndex(USER_ID_ONE, 12);
    }

    @After
    public void clearRepository() {
        taskList.clear();
        taskService.removeAll();
    }
    
}
