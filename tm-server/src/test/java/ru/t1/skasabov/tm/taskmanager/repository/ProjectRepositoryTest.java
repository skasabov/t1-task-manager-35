package ru.t1.skasabov.tm.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.skasabov.tm.api.repository.IProjectRepository;
import ru.t1.skasabov.tm.enumerated.ProjectSort;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_ONE = UUID.randomUUID().toString();

    private static final String USER_ID_TWO = UUID.randomUUID().toString();

    @NotNull private List<Project> projectList;

    @NotNull private IProjectRepository projectRepository;

    @Before
    public void initRepository() {
        projectList = new ArrayList<>();
        projectRepository = new ProjectRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("project " + i);
            project.setDescription("description " + i);
            if (i <= 5) project.setUserId(USER_ID_ONE);
            else project.setUserId(USER_ID_TWO);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @Test
    public void testAddProject() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Project project = new Project();
        projectRepository.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void createProjectName() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        projectRepository.create(userId, name);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        @Nullable final Project actualProject = projectRepository.findOneByIndex(projectRepository.getSize() - 1);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(userId, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
    }

    @Test
    public void createProject() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Description";
        projectRepository.create(userId, name, description);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        @Nullable final Project actualProject = projectRepository.findOneByIndex(projectRepository.getSize() - 1);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(userId, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Description";
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        @Nullable final Project actualProject = projectRepository.findOneByIndex(projectRepository.getSize() - 1);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(userId, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
    }

    @Test
    public void testAddAll() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<Project> projectList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            projectList.add(new Project());
        }
        projectRepository.addAll(projectList);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testAddNull() {
        projectRepository.addAll(null);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<Project> projectList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            projectList.add(new Project());
        }
        projectRepository.set(projectList);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testSetNull() {
        final int expectedNumberOfEntries = 0;
        projectRepository.set(null);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        projectRepository.removeAll();
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testClearAll() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES / 2;
        @NotNull final List<Project> actualProjectList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES / 2; i++) {
            actualProjectList.add(projectList.get(i));
        }
        projectRepository.removeAll(actualProjectList);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testClearForUserPositive() {
        @NotNull final List<Project> emptyList = new ArrayList<>();
        projectRepository.removeAll(USER_ID_ONE);
        Assert.assertEquals(emptyList, projectRepository.findAll(USER_ID_ONE));
        Assert.assertNotEquals(emptyList, projectRepository.findAll(USER_ID_TWO));
    }

    @Test
    public void testClearForUserNegative() {
        projectRepository.removeAll("other_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> actualProjectList = projectRepository.findAll();
        Assert.assertEquals(projectList, actualProjectList);
    }

    @Test
    public void testFindAllWithComparator() {
        @NotNull final List<Project> projects = projectRepository.findAll((Comparator<Project>) null);
        Assert.assertEquals(projectList, projects);
        @NotNull final Comparator<Project> comparator = ProjectSort.BY_NAME.getComparator();
        @NotNull final List<Project> actualProjectList = projectRepository.findAll(comparator);
        projectList.sort(comparator);
        Assert.assertEquals(projectList, actualProjectList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Project> actualProjectList = projectRepository.findAll(USER_ID_ONE);
        Assert.assertEquals(projectList.subList(0, 5), actualProjectList);
    }

    @Test
    public void testFindAllForUserWithComparator() {
        @NotNull final List<Project> userOneProjectList = projectList.subList(0, 5);
        @NotNull final List<Project> userOneProjects = projectRepository.findAll(USER_ID_ONE, null);
        Assert.assertEquals(userOneProjectList, userOneProjects);
        @NotNull final Comparator<Project> comparator = ProjectSort.BY_NAME.getComparator();
        @NotNull final List<Project> actualProjectList = projectRepository.findAll(USER_ID_ONE, comparator);
        userOneProjectList.sort(comparator);
        Assert.assertEquals(userOneProjectList, actualProjectList);
    }

    @Test
    public void testFindByIdPositive() {
        for (@NotNull final Project project : projectList) {
            Assert.assertNotNull(projectRepository.findOneById(project.getId()));
            Assert.assertEquals(project, projectRepository.findOneById(project.getId()));
        }
    }

    @Test
    public void testFindByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.findOneById(id));
    }

    @Test
    public void testFindByIdForUser() {
        for (@NotNull final Project project : projectList) {
            if (USER_ID_ONE.equals(project.getUserId())) {
                Assert.assertNotNull(projectRepository.findOneById(USER_ID_ONE, project.getId()));
                Assert.assertEquals(project, projectRepository.findOneById(project.getId()));
            }
            else
                Assert.assertNull(projectRepository.findOneById(USER_ID_ONE, project.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        @NotNull final Project expectedOne = projectList.get(0);
        @NotNull final Project expectedTwo = projectList.get(projectRepository.getSize() - 1);
        @NotNull final Project expectedThree = projectList.get(projectRepository.getSize() / 2);
        Assert.assertEquals(expectedOne, projectRepository.findOneByIndex(0));
        Assert.assertEquals(expectedTwo, projectRepository.findOneByIndex(projectRepository.getSize() - 1));
        Assert.assertEquals(expectedThree, projectRepository.findOneByIndex(projectRepository.getSize() / 2));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final Project expectedOne = projectList.get(0);
        @NotNull final Project expectedTwo = projectList.get(5);
        Assert.assertEquals(expectedOne, projectRepository.findOneByIndex(USER_ID_ONE, 0));
        Assert.assertEquals(expectedTwo, projectRepository.findOneByIndex(USER_ID_TWO, 0));
        Assert.assertNull(projectRepository.findOneByIndex("unknown user", 0));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(5, projectRepository.getSize(USER_ID_ONE));
        Assert.assertEquals(0, projectRepository.getSize("Unknown User"));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = projectList.get(0).getId();
        @NotNull final String invalidId = "some id";
        Assert.assertFalse(projectRepository.existsById(invalidId));
        Assert.assertTrue(projectRepository.existsById(validId));
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = projectList.get(0).getId();
        Assert.assertTrue(projectRepository.existsById(USER_ID_ONE, validId));
        Assert.assertFalse(projectRepository.existsById(USER_ID_TWO, validId));
    }

    @Test
    public void testRemovePositive() {
        for (@NotNull final Project project : projectList) {
            projectRepository.removeOne(project);
            Assert.assertNull(projectRepository.findOneById(project.getId()));
        }
    }

    @Test
    public void testRemoveNegative() {
        @NotNull final Project projectNotFromRepository = new Project();
        projectRepository.removeOne(projectNotFromRepository);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testRemoveForUser() {
        for (@NotNull final Project project : projectList) {
            if (USER_ID_ONE.equals(project.getUserId())) {
                Assert.assertNotNull(projectRepository.removeOne(USER_ID_ONE, project));
                Assert.assertNull(projectRepository.findOneById(USER_ID_ONE, project.getId()));
            }
            else {
                Assert.assertThrows(ModelEmptyException.class, () -> projectRepository.removeOne(
                        USER_ID_ONE, project
                ));
            }
        }
    }

    @Test
    public void testRemoveByIdPositive() {
        for (@NotNull final Project project : projectList) {
            Assert.assertNotNull(projectRepository.removeOneById(project.getId()));
            Assert.assertNull(projectRepository.findOneById(project.getId()));
        }
    }

    @Test
    public void testRemoveByIdNegative() {
        @NotNull final String idNotFromRepository = UUID.randomUUID().toString();
        Assert.assertThrows(ModelEmptyException.class, () -> projectRepository.removeOneById(idNotFromRepository));
    }

    @Test
    public void testRemoveByIdForUser() {
        for (@NotNull final Project project : projectList) {
            if (USER_ID_ONE.equals(project.getUserId())) {
                Assert.assertNotNull(projectRepository.removeOneById(USER_ID_ONE, project.getId()));
                Assert.assertNull(projectRepository.findOneById(USER_ID_ONE, project.getId()));
            }
            else {
                Assert.assertThrows(ModelEmptyException.class, () -> projectRepository.removeOneById(
                        USER_ID_ONE, project.getId()
                ));
            }
        }
    }

    @Test
    public void testRemoveByIndex() {
        @NotNull final Project expectedOne = projectList.get(0);
        @NotNull final Project expectedTwo = projectList.get(projectRepository.getSize() - 1);
        @NotNull final Project expectedThree = projectList.get(projectRepository.getSize() / 2);
        Assert.assertEquals(expectedOne, projectRepository.removeOneByIndex(0));
        Assert.assertEquals(expectedTwo, projectRepository.removeOneByIndex(projectRepository.getSize() - 1));
        Assert.assertEquals(expectedThree, projectRepository.removeOneByIndex(projectRepository.getSize() / 2));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 3, projectRepository.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        @NotNull final Project expectedOne = projectList.get(0);
        @NotNull final Project expectedTwo = projectList.get(5);
        Assert.assertEquals(expectedOne, projectRepository.removeOneByIndex(USER_ID_ONE, 0));
        Assert.assertEquals(expectedTwo, projectRepository.removeOneByIndex(USER_ID_TWO, 0));
        Assert.assertThrows(ModelEmptyException.class, () -> projectRepository.removeOneByIndex(
                "unknown user", 0
        ));
    }

    @After
    public void clearRepository() {
        projectList.clear();
        projectRepository.removeAll();
    }

}
