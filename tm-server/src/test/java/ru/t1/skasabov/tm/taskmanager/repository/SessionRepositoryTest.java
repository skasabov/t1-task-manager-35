package ru.t1.skasabov.tm.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.skasabov.tm.api.repository.ISessionRepository;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.model.Session;
import ru.t1.skasabov.tm.repository.SessionRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_ONE = UUID.randomUUID().toString();

    private static final String USER_ID_TWO = UUID.randomUUID().toString();

    @NotNull private List<Session> sessionList;

    @NotNull private ISessionRepository sessionRepository;

    @Before
    public void initRepository() {
        sessionList = new ArrayList<>();
        sessionRepository = new SessionRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i <= 5) session.setUserId(USER_ID_ONE);
            else session.setUserId(USER_ID_TWO);
            sessionRepository.add(session);
            sessionList.add(session);
        }
    }

    @Test
    public void testAddSession() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Session session = new Session();
        sessionRepository.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final Session session = new Session();
        sessionRepository.add(userId, session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
        @Nullable final Session actualSession = sessionRepository.findOneByIndex(sessionRepository.getSize() - 1);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(userId, actualSession.getUserId());
    }

    @Test
    public void testAddAll() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<Session> sessionList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            sessionList.add(new Session());
        }
        sessionRepository.addAll(sessionList);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testAddNull() {
        sessionRepository.addAll(null);
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<Session> sessionList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            sessionList.add(new Session());
        }
        sessionRepository.set(sessionList);
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testSetNull() {
        final int expectedNumberOfEntries = 0;
        sessionRepository.set(null);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        sessionRepository.removeAll();
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testClearAll() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES / 2;
        @NotNull final List<Session> actualSessionList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES / 2; i++) {
            actualSessionList.add(sessionList.get(i));
        }
        sessionRepository.removeAll(actualSessionList);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testClearForUserPositive() {
        @NotNull final List<Session> emptyList = new ArrayList<>();
        sessionRepository.removeAll(USER_ID_ONE);
        Assert.assertEquals(emptyList, sessionRepository.findAll(USER_ID_ONE));
        Assert.assertNotEquals(emptyList, sessionRepository.findAll(USER_ID_TWO));
    }

    @Test
    public void testClearForUserNegative() {
        sessionRepository.removeAll("other_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Session> actualSessionList = sessionRepository.findAll();
        Assert.assertEquals(sessionList, actualSessionList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Session> actualSessionList = sessionRepository.findAll(USER_ID_ONE);
        Assert.assertEquals(sessionList.subList(0, 5), actualSessionList);
    }

    @Test
    public void testFindByIdPositive() {
        for (@NotNull final Session session : sessionList) {
            Assert.assertNotNull(sessionRepository.findOneById(session.getId()));
            Assert.assertEquals(session, sessionRepository.findOneById(session.getId()));
        }
    }

    @Test
    public void testFindByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(sessionRepository.findOneById(id));
    }

    @Test
    public void testFindByIdForUser() {
        for (@NotNull final Session session : sessionList) {
            if (USER_ID_ONE.equals(session.getUserId())) {
                Assert.assertNotNull(sessionRepository.findOneById(USER_ID_ONE, session.getId()));
                Assert.assertEquals(session, sessionRepository.findOneById(session.getId()));
            }
            else
                Assert.assertNull(sessionRepository.findOneById(USER_ID_ONE, session.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        @NotNull final Session expectedOne = sessionList.get(0);
        @NotNull final Session expectedTwo = sessionList.get(sessionRepository.getSize() - 1);
        @NotNull final Session expectedThree = sessionList.get(sessionRepository.getSize() / 2);
        Assert.assertEquals(expectedOne, sessionRepository.findOneByIndex(0));
        Assert.assertEquals(expectedTwo, sessionRepository.findOneByIndex(sessionRepository.getSize() - 1));
        Assert.assertEquals(expectedThree, sessionRepository.findOneByIndex(sessionRepository.getSize() / 2));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final Session expectedOne = sessionList.get(0);
        @NotNull final Session expectedTwo = sessionList.get(5);
        Assert.assertEquals(expectedOne, sessionRepository.findOneByIndex(USER_ID_ONE, 0));
        Assert.assertEquals(expectedTwo, sessionRepository.findOneByIndex(USER_ID_TWO, 0));
        Assert.assertNull(sessionRepository.findOneByIndex("unknown user", 0));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(5, sessionRepository.getSize(USER_ID_ONE));
        Assert.assertEquals(0, sessionRepository.getSize("Unknown User"));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = sessionList.get(0).getId();
        @NotNull final String invalidId = "some id";
        Assert.assertFalse(sessionRepository.existsById(invalidId));
        Assert.assertTrue(sessionRepository.existsById(validId));
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = sessionList.get(0).getId();
        Assert.assertTrue(sessionRepository.existsById(USER_ID_ONE, validId));
        Assert.assertFalse(sessionRepository.existsById(USER_ID_TWO, validId));
    }

    @Test
    public void testRemovePositive() {
        for (@NotNull final Session session : sessionList) {
            sessionRepository.removeOne(session);
            Assert.assertNull(sessionRepository.findOneById(session.getId()));
        }
    }

    @Test
    public void testRemoveNegative() {
        @NotNull final Session sessionNotFromRepository = new Session();
        sessionRepository.removeOne(sessionNotFromRepository);
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testRemoveForUser() {
        for (@NotNull final Session session : sessionList) {
            if (USER_ID_ONE.equals(session.getUserId())) {
                Assert.assertNotNull(sessionRepository.removeOne(USER_ID_ONE, session));
                Assert.assertNull(sessionRepository.findOneById(USER_ID_ONE, session.getId()));
            }
            else {
                Assert.assertThrows(ModelEmptyException.class, () -> sessionRepository.removeOne(
                        USER_ID_ONE, session
                ));
            }
        }
    }

    @Test
    public void testRemoveByIdPositive() {
        for (@NotNull final Session session : sessionList) {
            Assert.assertNotNull(sessionRepository.removeOneById(session.getId()));
            Assert.assertNull(sessionRepository.findOneById(session.getId()));
        }
    }

    @Test
    public void testRemoveByIdNegative() {
        @NotNull final String idNotFromRepository = UUID.randomUUID().toString();
        Assert.assertThrows(ModelEmptyException.class, () -> sessionRepository.removeOneById(idNotFromRepository));
    }

    @Test
    public void testRemoveByIdForUser() {
        for (@NotNull final Session session : sessionList) {
            if (USER_ID_ONE.equals(session.getUserId())) {
                Assert.assertNotNull(sessionRepository.removeOneById(USER_ID_ONE, session.getId()));
                Assert.assertNull(sessionRepository.findOneById(USER_ID_ONE, session.getId()));
            }
            else {
                Assert.assertThrows(ModelEmptyException.class, () -> sessionRepository.removeOneById(
                        USER_ID_ONE, session.getId()
                ));
            }
        }
    }

    @Test
    public void testRemoveByIndex() {
        @NotNull final Session expectedOne = sessionList.get(0);
        @NotNull final Session expectedTwo = sessionList.get(sessionRepository.getSize() - 1);
        @NotNull final Session expectedThree = sessionList.get(sessionRepository.getSize() / 2);
        Assert.assertEquals(expectedOne, sessionRepository.removeOneByIndex(0));
        Assert.assertEquals(expectedTwo, sessionRepository.removeOneByIndex(sessionRepository.getSize() - 1));
        Assert.assertEquals(expectedThree, sessionRepository.removeOneByIndex(sessionRepository.getSize() / 2));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 3, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        @NotNull final Session expectedOne = sessionList.get(0);
        @NotNull final Session expectedTwo = sessionList.get(5);
        Assert.assertEquals(expectedOne, sessionRepository.removeOneByIndex(USER_ID_ONE, 0));
        Assert.assertEquals(expectedTwo, sessionRepository.removeOneByIndex(USER_ID_TWO, 0));
        Assert.assertThrows(ModelEmptyException.class, () -> sessionRepository.removeOneByIndex(
                "unknown user", 0
        ));
    }

    @After
    public void clearRepository() {
        sessionList.clear();
        sessionRepository.removeAll();
    }

}
