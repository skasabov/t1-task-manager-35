package ru.t1.skasabov.tm.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.skasabov.tm.api.repository.IProjectRepository;
import ru.t1.skasabov.tm.api.service.IProjectService;
import ru.t1.skasabov.tm.enumerated.ProjectSort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.repository.ProjectRepository;
import ru.t1.skasabov.tm.service.ProjectService;

import java.util.*;

public class ProjectServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_ONE = UUID.randomUUID().toString();

    private static final String USER_ID_TWO = UUID.randomUUID().toString();

    @NotNull private List<Project> projectList;

    @NotNull private IProjectService projectService;

    @Before
    public void initTest() {
        projectList = new ArrayList<>();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        projectService = new ProjectService(projectRepository);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Test " + i);
            if (i <= 5) project.setUserId(USER_ID_ONE);
            else project.setUserId(USER_ID_TWO);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateForEmptyUser() {
        projectService.create("", "test", "");
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateEmptyName() {
        @NotNull final String userId = UUID.randomUUID().toString();
        projectService.create(userId, "", "");
    }

    @Test
    public void testCreateName() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        projectService.create(userId, name, "");
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
        @Nullable final Project actualProject = projectService.findOneByIndex(projectService.getSize() - 1);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(userId, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
    }

    @Test
    public void createProject() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Description";
        @NotNull final Date dateBegin = new Date();
        @NotNull final Date dateEnd = new Date();
        projectService.create(userId, name, description, dateBegin, dateEnd);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
        @Nullable final Project actualProject = projectService.findOneByIndex(projectService.getSize() - 1);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(userId, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
        Assert.assertEquals(dateBegin, actualProject.getDateBegin());
        Assert.assertEquals(dateEnd, actualProject.getDateEnd());
    }

    @Test
    public void testUpdateById() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @Nullable final String id = projectService.findAll().get(0).getId();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        @NotNull final Project project = projectService.updateById(userId, id, name, description);
        Assert.assertNotNull(project);
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByEmptyId() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateById(userId, "", name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIdForEmptyUser() {
        @Nullable final String id = projectService.findAll().get(0).getId();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateById("", id, name, description);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdWithEmptyName() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @Nullable final String id = projectService.findAll().get(0).getId();
        @NotNull final String description = "Test Description One";
        projectService.updateById(userId, id, "", description);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUpdateByIdProjectNotFound() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @Nullable final String id = projectService.findAll().get(7).getId();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateById(userId, id, name, description);
    }

    @Test
    public void testUpdateByIndex() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        @NotNull final Project project = projectService.updateByIndex(userId, 0, name, description);
        Assert.assertNotNull(project);
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByEmptyIndex() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex(userId, null, name, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndexForEmptyUser() {
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex("", 0, name, description);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndexWithEmptyName() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex(userId, 0, "", description);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByNegativeIndex() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex(userId, -2, name, description);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIncorrectIndex() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @NotNull final String name = "Test Project One";
        @NotNull final String description = "Test Description One";
        projectService.updateByIndex(userId, 7, name, description);
    }

    @Test
    public void testChangeProjectStatusById() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @Nullable final String id = projectService.findAll().get(0).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        @NotNull final Project project = projectService.changeProjectStatusById(userId, id, status);
        Assert.assertNotNull(project);
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertEquals(status, project.getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeProjectStatusByEmptyId() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusById(userId, "", status);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeProjectStatusByIdForEmptyUser() {
        @Nullable final String id = projectService.findAll().get(0).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusById("", id, status);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeProjectStatusByIdWithEmptyStatus() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @Nullable final String id = projectService.findAll().get(0).getId();
        projectService.changeProjectStatusById(userId, id, Status.toStatus(null));
    }

    @Test(expected = StatusIncorrectException.class)
    public void testChangeProjectStatusByIdWithIncorrectStatus() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @Nullable final String id = projectService.findAll().get(0).getId();
        projectService.changeProjectStatusById(userId, id, Status.toStatus("123"));
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeProjectStatusByIdProjectNotFound() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @Nullable final String id = projectService.findAll().get(7).getId();
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusById(userId, id, status);
    }

    @Test
    public void testChangeProjectStatusByIndex() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @NotNull final Status status = Status.NOT_STARTED;
        @NotNull final Project project = projectService.changeProjectStatusByIndex(userId, 0, status);
        Assert.assertNotNull(project);
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertEquals(status, project.getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeProjectStatusByEmptyIndex() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusByIndex(userId, null, status);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeProjectStatusByIndexForEmptyUser() {
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusByIndex("", 0, status);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeProjectStatusByIndexWithEmptyStatus() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        projectService.changeProjectStatusByIndex(userId, 0, Status.toStatus(null));
    }

    @Test(expected = StatusIncorrectException.class)
    public void testChangeProjectStatusByIndexWithIncorrectStatus() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        projectService.changeProjectStatusByIndex(userId, 0, Status.toStatus("123"));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeProjectStatusByIncorrectIndex() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusByIndex(userId, 7, status);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeProjectStatusByNegativeIndex() {
        @Nullable final String userId = projectService.findAll().get(0).getUserId();
        @NotNull final Status status = Status.NOT_STARTED;
        projectService.changeProjectStatusByIndex(userId, -2, status);
    }

    @Test
    public void testAdd() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
        projectService.add(new Project());
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, projectService.getSize());
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddNull() {
        projectService.add(null);
    }

    @Test
    public void testAddForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, projectService.getSize(USER_ID_ONE));
        Project project = new Project();
        project.setUserId(USER_ID_ONE);
        projectService.add(project);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectService.getSize(USER_ID_ONE));
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddNullForUser() {
        projectService.add(USER_ID_ONE, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testAddForEmptyUser() {
        Project project = new Project();
        project.setUserId(USER_ID_ONE);
        projectService.add("", project);
    }

    @Test
    public void testAddAll() {
        @NotNull final List<Project> actualProjects = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            actualProjects.add(new Project());
        }
        projectService.addAll(actualProjects);
        Assert.assertEquals(NUMBER_OF_ENTRIES + 4, projectService.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<Project> actualProjects = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            actualProjects.add(new Project());
        }
        projectService.set(actualProjects);
        Assert.assertEquals(10, projectService.getSize());
    }

    @Test
    public void testClearAll() {
        projectService.removeAll();
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void testClearAllForUser() {
        projectService.removeAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, projectService.getSize());
    }

    @Test
    public void testClear() {
        projectService.removeAll(projectList.subList(0, 5));
        Assert.assertEquals(5, projectService.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> projectList = projectService.findAll();
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectList.size());
    }

    @Test
    public void testFindAllWithComparator() {
        @NotNull final ProjectSort sort = ProjectSort.BY_NAME;
        @NotNull final List<Project> projectSortList = projectService.findAll(sort.getComparator());
        Assert.assertNotEquals(projectList, projectSortList);
    }

    @Test
    public void testFindAllWithEmptyComparator() {
        @NotNull final List<Project> projectSortList = projectService.findAll((Comparator<Project>) null);
        Assert.assertEquals(projectSortList, projectList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Project> projectList = projectService.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, projectList.size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllForEmptyUser() {
        projectService.findAll("");
    }

    @Test
    public void testFindAllWithComparatorForUser() {
        @NotNull final ProjectSort sort = ProjectSort.BY_NAME;
        @NotNull final List<Project> projectSortList = projectService.findAll(USER_ID_ONE, sort.getComparator());
        Assert.assertEquals(projectList.subList(0, 5), projectSortList);
    }

    @Test
    public void testFindAllWithEmptyComparatorForUser() {
        @NotNull final List<Project> projectSortList = projectService.findAll(USER_ID_ONE, null);
        Assert.assertEquals(projectList.subList(0, 5), projectSortList);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllWithComparatorForEmptyUser() {
        @NotNull final ProjectSort sort = ProjectSort.BY_NAME;
        projectService.findAll("", sort.getComparator());
    }

    @Test
    public void testFindById() {
        @NotNull final String projectId = projectService.findAll().get(0).getId();
        Assert.assertEquals(projectList.get(0), projectService.findOneById(projectId));
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyId() {
        Assert.assertNull(projectService.findOneById(""));
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final String projectId = projectService.findAll().get(0).getId();
        Assert.assertEquals(projectList.get(0), projectService.findOneById(USER_ID_ONE, projectId));
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyIdForUser() {
        Assert.assertNull(projectService.findOneById(USER_ID_ONE, ""));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIdForEmptyUser() {
        @NotNull final String projectId = projectService.findAll().get(0).getId();
        projectService.findOneById("", projectId);
    }

    @Test
    public void testFindByIdProjectNotFound() {
        Assert.assertNull(projectService.findOneById("some id"));
    }

    @Test
    public void testFindByIdProjectNotFoundForUser() {
        String projectId = projectList.get(7).getId();
        Assert.assertNull(projectService.findOneById(USER_ID_ONE, projectId));
    }

    @Test
    public void testFindByIndex() {
        Assert.assertEquals(projectList.get(0), projectService.findOneByIndex(0));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndex() {
        projectService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndex() {
        projectService.findOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndex() {
        projectService.findOneByIndex(12);
    }

    @Test
    public void testFindByIndexForUser() {
        Assert.assertEquals(projectList.get(0), projectService.findOneByIndex(USER_ID_ONE, 0));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndexForUser() {
        projectService.findOneByIndex(USER_ID_ONE, null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndexForUser() {
        projectService.findOneByIndex(USER_ID_ONE, -2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndexForUser() {
        projectService.findOneByIndex(USER_ID_ONE, 12);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIndexForEmptyUser() {
        projectService.findOneByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexProjectNotFound() {
        projectService.findOneByIndex(USER_ID_ONE, 7);
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
        projectService.add(new Project());
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, projectService.getSize());
        projectService.removeAll();
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, projectService.getSize(USER_ID_ONE));
        Project project = new Project();
        project.setUserId(USER_ID_ONE);
        projectService.add(project);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectService.getSize(USER_ID_ONE));
        projectService.removeAll(USER_ID_ONE);
        Assert.assertEquals(0, projectService.getSize(USER_ID_ONE));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testGetSizeForEmptyUser() {
        projectService.getSize("");
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = projectList.get(0).getId();
        @NotNull final String invalidId = "some id";
        Assert.assertFalse(projectService.existsById(invalidId));
        Assert.assertTrue(projectService.existsById(validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyId() {
        projectService.existsById("");
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = projectList.get(0).getId();
        @NotNull final String invalidId = projectList.get(7).getId();
        Assert.assertFalse(projectService.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(projectService.existsById(USER_ID_ONE, validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyIdForUser() {
        projectService.existsById(USER_ID_ONE, "");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testIsNotFoundForEmptyUser() {
        projectService.existsById("", projectList.get(0).getId());
    }

    @Test
    public void testRemove() {
        @NotNull final Project project = projectService.findAll().get(3);
        projectService.removeOne(project);
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, projectService.getSize());
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveNull() {
        projectService.removeOne(null);
    }

    @Test
    public void testRemoveForUser() {
        @NotNull final Project project = projectService.findAll().get(3);
        projectService.removeOne(USER_ID_ONE, project);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 - 1, projectService.getSize(USER_ID_ONE));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveForEmptyUser() {
        @NotNull final Project project = projectService.findAll().get(3);
        projectService.removeOne("", project);
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveNullForUser() {
        projectService.removeOne(USER_ID_ONE, null);
    }

    @Test
    public void testRemoveById() {
        @NotNull final String projectId = projectService.findAll().get(3).getId();
        projectService.removeOneById(projectId);
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, projectService.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        @NotNull final String projectId = projectService.findAll().get(3).getId();
        projectService.removeOneById(USER_ID_ONE, projectId);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 - 1, projectService.getSize(USER_ID_ONE));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyId() {
        Assert.assertNull(projectService.removeOneById(""));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyIdForUser() {
        projectService.removeOneById(USER_ID_ONE, "");
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveByIdProjectNotFound() {
        projectService.removeOneById("some id");
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveByIdProjectNotFoundForUser() {
        String projectId = projectList.get(7).getId();
        projectService.removeOneById(USER_ID_ONE, projectId);
    }

    @Test
    public void testRemoveByIndex() {
        Assert.assertEquals(projectList.get(0), projectService.removeOneByIndex(0));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByEmptyIndex() {
        projectService.removeOneByIndex(null);
    }

    @Test
    public void testRemoveByIndexForUser() {
        Assert.assertEquals(projectList.get(0), projectService.removeOneByIndex(USER_ID_ONE, 0));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByEmptyIndexForUser() {
        projectService.removeOneByIndex(USER_ID_ONE, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveByIndexForEmptyUser() {
        projectService.removeOneByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexProjectNotFound() {
        projectService.removeOneByIndex(USER_ID_ONE, 7);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndex() {
        projectService.removeOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndexForUser() {
        projectService.removeOneByIndex(USER_ID_ONE, -2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndex() {
        projectService.removeOneByIndex(12);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndexForUser() {
        projectService.removeOneByIndex(USER_ID_ONE, 12);
    }

    @After
    public void clearRepository() {
        projectList.clear();
        projectService.removeAll();
    }
    
}
