package ru.t1.skasabov.tm.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.skasabov.tm.api.repository.ITaskRepository;
import ru.t1.skasabov.tm.enumerated.TaskSort;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_ONE = UUID.randomUUID().toString();

    private static final String USER_ID_TWO = UUID.randomUUID().toString();

    @NotNull private final Project projectOne = new Project();

    @NotNull private final Project projectTwo = new Project();

    @NotNull private List<Task> taskList;

    @NotNull private ITaskRepository taskRepository;

    @Before
    public void initRepository() {
        taskList = new ArrayList<>();
        taskRepository = new TaskRepository();
        projectOne.setName("Test One Project");
        projectOne.setDescription("Test One Description");
        projectTwo.setName("Test Two Project");
        projectTwo.setDescription("Test Two Description");
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("task " + i);
            task.setDescription("description " + i);
            if (i <= 5) {
                task.setUserId(USER_ID_ONE);
                task.setProjectId(projectOne.getId());
            }
            else {
                task.setUserId(USER_ID_TWO);
                task.setProjectId(projectTwo.getId());
            }
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void testAddTask() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Task task = new Task();
        taskRepository.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void createTaskName() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Task";
        taskRepository.create(userId, name);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final Task actualTask = taskRepository.findOneByIndex(taskRepository.getSize() - 1);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(userId, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
    }

    @Test
    public void createTask() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Task";
        @NotNull final String description = "Test Description";
        taskRepository.create(userId, name, description);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final Task actualTask = taskRepository.findOneByIndex(taskRepository.getSize() - 1);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(userId, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test task";
        @NotNull final String description = "Test Description";
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final Task actualTask = taskRepository.findOneByIndex(taskRepository.getSize() - 1);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(userId, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
    }

    @Test
    public void testAddAll() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<Task> taskList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            taskList.add(new Task());
        }
        taskRepository.addAll(taskList);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testAddNull() {
        taskRepository.addAll(null);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<Task> taskList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            taskList.add(new Task());
        }
        taskRepository.set(taskList);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testSetNull() {
        final int expectedNumberOfEntries = 0;
        taskRepository.set(null);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        taskRepository.removeAll();
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testClearAll() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES / 2;
        @NotNull final List<Task> actualTaskList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES / 2; i++) {
            actualTaskList.add(taskList.get(i));
        }
        taskRepository.removeAll(actualTaskList);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testClearForUserPositive() {
        @NotNull final List<Task> emptyList = new ArrayList<>();
        taskRepository.removeAll(USER_ID_ONE);
        Assert.assertEquals(emptyList, taskRepository.findAll(USER_ID_ONE));
        Assert.assertNotEquals(emptyList, taskRepository.findAll(USER_ID_TWO));
    }

    @Test
    public void testClearForUserNegative() {
        taskRepository.removeAll("other_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> actualTaskList = taskRepository.findAll();
        Assert.assertEquals(taskList, actualTaskList);
    }

    @Test
    public void testFindAllWithComparator() {
        @NotNull final List<Task> tasks = taskRepository.findAll((Comparator<Task>) null);
        Assert.assertEquals(taskList, tasks);
        @NotNull final Comparator<Task> comparator = TaskSort.BY_NAME.getComparator();
        @NotNull final List<Task> actualTaskList = taskRepository.findAll(comparator);
        taskList.sort(comparator);
        Assert.assertEquals(taskList, actualTaskList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Task> actualTaskList = taskRepository.findAll(USER_ID_ONE);
        Assert.assertEquals(taskList.subList(0, 5), actualTaskList);
    }

    @Test
    public void testFindAllForUserWithComparator() {
        @NotNull final List<Task> userOneTaskList = taskList.subList(0, 5);
        @NotNull final List<Task> userOneTasks = taskRepository.findAll(USER_ID_ONE, null);
        Assert.assertEquals(userOneTaskList, userOneTasks);
        @NotNull final Comparator<Task> comparator = TaskSort.BY_NAME.getComparator();
        @NotNull final List<Task> actualTaskList = taskRepository.findAll(USER_ID_ONE, comparator);
        userOneTaskList.sort(comparator);
        Assert.assertEquals(userOneTaskList, actualTaskList);
    }

    @Test
    public void testFindAllTasksByProjectId() {
        @NotNull final List<Task> tasksProjectOne = taskRepository.findAllByProjectId(USER_ID_ONE, projectOne.getId());
        @NotNull final List<Task> tasksProjectTwo = taskRepository.findAllByProjectId(USER_ID_TWO, projectTwo.getId());
        Assert.assertEquals(taskList.subList(0, 5), tasksProjectOne);
        Assert.assertEquals(taskList.subList(5, 10), tasksProjectTwo);
    }

    @Test
    public void testFindByIdPositive() {
        for (@NotNull final Task task : taskList) {
            Assert.assertNotNull(taskRepository.findOneById(task.getId()));
            Assert.assertEquals(task, taskRepository.findOneById(task.getId()));
        }
    }

    @Test
    public void testFindByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskRepository.findOneById(id));
    }

    @Test
    public void testFindByIdForUser() {
        for (@NotNull final Task task : taskList) {
            if (USER_ID_ONE.equals(task.getUserId())) {
                Assert.assertNotNull(taskRepository.findOneById(USER_ID_ONE, task.getId()));
                Assert.assertEquals(task, taskRepository.findOneById(task.getId()));
            }
            else
                Assert.assertNull(taskRepository.findOneById(USER_ID_ONE, task.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        @NotNull final Task expectedOne = taskList.get(0);
        @NotNull final Task expectedTwo = taskList.get(taskRepository.getSize() - 1);
        @NotNull final Task expectedThree = taskList.get(taskRepository.getSize() / 2);
        Assert.assertEquals(expectedOne, taskRepository.findOneByIndex(0));
        Assert.assertEquals(expectedTwo, taskRepository.findOneByIndex(taskRepository.getSize() - 1));
        Assert.assertEquals(expectedThree, taskRepository.findOneByIndex(taskRepository.getSize() / 2));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final Task expectedOne = taskList.get(0);
        @NotNull final Task expectedTwo = taskList.get(5);
        Assert.assertEquals(expectedOne, taskRepository.findOneByIndex(USER_ID_ONE, 0));
        Assert.assertEquals(expectedTwo, taskRepository.findOneByIndex(USER_ID_TWO, 0));
        Assert.assertNull(taskRepository.findOneByIndex("unknown user", 0));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(5, taskRepository.getSize(USER_ID_ONE));
        Assert.assertEquals(0, taskRepository.getSize("Unknown User"));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = taskList.get(0).getId();
        @NotNull final String invalidId = "some id";
        Assert.assertFalse(taskRepository.existsById(invalidId));
        Assert.assertTrue(taskRepository.existsById(validId));
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = taskList.get(0).getId();
        Assert.assertTrue(taskRepository.existsById(USER_ID_ONE, validId));
        Assert.assertFalse(taskRepository.existsById(USER_ID_TWO, validId));
    }

    @Test
    public void testRemovePositive() {
        for (@NotNull final Task task : taskList) {
            taskRepository.removeOne(task);
            Assert.assertNull(taskRepository.findOneById(task.getId()));
        }
    }

    @Test
    public void testRemoveNegative() {
        @NotNull final Task taskNotFromRepository = new Task();
        taskRepository.removeOne(taskNotFromRepository);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testRemoveForUser() {
        for (@NotNull final Task task : taskList) {
            if (USER_ID_ONE.equals(task.getUserId())) {
                Assert.assertNotNull(taskRepository.removeOne(USER_ID_ONE, task));
                Assert.assertNull(taskRepository.findOneById(USER_ID_ONE, task.getId()));
            }
            else {
                Assert.assertThrows(ModelEmptyException.class, () -> taskRepository.removeOne(
                        USER_ID_ONE, task
                ));
            }
        }
    }

    @Test
    public void testRemoveByIdPositive() {
        for (@NotNull final Task task : taskList) {
            Assert.assertNotNull(taskRepository.removeOneById(task.getId()));
            Assert.assertNull(taskRepository.findOneById(task.getId()));
        }
    }

    @Test
    public void testRemoveByIdNegative() {
        @NotNull final String idNotFromRepository = UUID.randomUUID().toString();
        Assert.assertThrows(ModelEmptyException.class, () -> taskRepository.removeOneById(idNotFromRepository));
    }

    @Test
    public void testRemoveByIdForUser() {
        for (@NotNull final Task task : taskList) {
            if (USER_ID_ONE.equals(task.getUserId())) {
                Assert.assertNotNull(taskRepository.removeOneById(USER_ID_ONE, task.getId()));
                Assert.assertNull(taskRepository.findOneById(USER_ID_ONE, task.getId()));
            }
            else {
                Assert.assertThrows(ModelEmptyException.class, () -> taskRepository.removeOneById(
                        USER_ID_ONE, task.getId()
                ));
            }
        }
    }

    @Test
    public void testRemoveByIndex() {
        @NotNull final Task expectedOne = taskList.get(0);
        @NotNull final Task expectedTwo = taskList.get(taskRepository.getSize() - 1);
        @NotNull final Task expectedThree = taskList.get(taskRepository.getSize() / 2);
        Assert.assertEquals(expectedOne, taskRepository.removeOneByIndex(0));
        Assert.assertEquals(expectedTwo, taskRepository.removeOneByIndex(taskRepository.getSize() - 1));
        Assert.assertEquals(expectedThree, taskRepository.removeOneByIndex(taskRepository.getSize() / 2));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 3, taskRepository.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        @NotNull final Task expectedOne = taskList.get(0);
        @NotNull final Task expectedTwo = taskList.get(5);
        Assert.assertEquals(expectedOne, taskRepository.removeOneByIndex(USER_ID_ONE, 0));
        Assert.assertEquals(expectedTwo, taskRepository.removeOneByIndex(USER_ID_TWO, 0));
        Assert.assertThrows(ModelEmptyException.class, () -> taskRepository.removeOneByIndex(
                "unknown user", 0
        ));
    }

    @After
    public void clearRepository() {
        taskList.clear();
        taskRepository.removeAll();
    }

}
