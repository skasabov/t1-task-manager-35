package ru.t1.skasabov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.service.IPropertyService;

import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull private static final String FILE_NAME = "application.properties";

    @NotNull private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull private static final String SERVER_HOST_KEY = "server.host";

    @NotNull private static final String SERVER_PORT_KEY = "server.port";

    @NotNull private static final String SESSION_KEY = "session.key";

    @NotNull private static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull private static final String PASSWORD_ITERATION_DEFAULT = "25456";

    @NotNull private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull private static final String PASSWORD_SECRET_DEFAULT = "356585985";

    @NotNull private static final String SERVER_HOST_KEY_DEFAULT = "0.0.0.0";

    @NotNull private static final String SERVER_PORT_KEY_DEFAULT = "8080";

    @NotNull private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull private static final String SESSION_KEY_DEFAULT = "5363453453";

    @NotNull private static final String SESSION_TIMEOUT_KEY_DEFAULT = "10800";

    @NotNull private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT_KEY, SESSION_TIMEOUT_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public String getHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public String getPort() {
        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_KEY_DEFAULT);
    }

    @NotNull
    private String envKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = envKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

}
