package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.Task;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    Project removeProjectById(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

}
