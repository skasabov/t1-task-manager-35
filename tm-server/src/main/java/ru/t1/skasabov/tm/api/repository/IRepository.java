package ru.t1.skasabov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator);

    @Nullable
    M add(@Nullable M model);

    @NotNull
    Collection<M> addAll(@Nullable Collection<M> models);

    @NotNull
    Collection<M> set(@Nullable Collection<M> models);

    @NotNull
    Boolean existsById(@NotNull String id);

    @Nullable
    M findOneById(@Nullable String id);

    @NotNull
    M findOneByIndex(@Nullable Integer index);

    int getSize();

    @Nullable
    M removeOne(@Nullable M model);

    @Nullable
    M removeOneById(@NotNull String id);

    @Nullable
    M removeOneByIndex(@Nullable Integer index);

    void removeAll(@NotNull Collection<M> collection);

    void removeAll();

}
