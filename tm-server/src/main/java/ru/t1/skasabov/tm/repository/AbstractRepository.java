package ru.t1.skasabov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.repository.IRepository;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.field.IdEmptyException;
import ru.t1.skasabov.tm.exception.field.IndexIncorrectException;
import ru.t1.skasabov.tm.model.AbstractModel;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull protected final List<M> records = new ArrayList<>();

    @Override
    public void removeAll(@NotNull final Collection<M> models) {
        records.removeAll(models);
    }

    @Nullable
    @Override
    public M add(@Nullable final M model) {
        if (model == null) return null;
        records.add(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> addAll(@Nullable final Collection<M> models) {
        if (models == null) return Collections.emptyList();
        records.addAll(models);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@Nullable final Collection<M> models) {
        removeAll();
        return addAll(models);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return records;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return records
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return records
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        return records.get(index);
    }

    @Nullable
    @Override
    public M removeOne(@Nullable final M model) {
        records.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) throw new ModelEmptyException();
        records.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final Integer index) {
        @NotNull final M model = findOneByIndex(index);
        records.remove(model);
        return model;
    }

    @Override
    public void removeAll() {
        records.clear();
    }

    @Override
    public int getSize() {
        return records.size();
    }

    @NotNull
    @Override
    public Boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

}
