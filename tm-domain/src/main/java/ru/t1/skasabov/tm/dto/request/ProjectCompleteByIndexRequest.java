package ru.t1.skasabov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCompleteByIndexRequest extends AbstractUserRequest {

    @NotNull private Integer index;

    public ProjectCompleteByIndexRequest(@NotNull final Integer index) {
        this.index = index;
    }

}
