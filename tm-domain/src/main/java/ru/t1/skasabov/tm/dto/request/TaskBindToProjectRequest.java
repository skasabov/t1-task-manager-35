package ru.t1.skasabov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskBindToProjectRequest extends AbstractUserRequest {

    @Nullable private String projectId;

    @Nullable private String taskId;

    public TaskBindToProjectRequest(@Nullable final String projectId, @Nullable final String taskId) {
        this.projectId = projectId;
        this.taskId = taskId;
    }

}
