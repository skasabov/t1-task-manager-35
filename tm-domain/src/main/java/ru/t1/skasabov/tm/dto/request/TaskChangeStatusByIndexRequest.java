package ru.t1.skasabov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class TaskChangeStatusByIndexRequest extends AbstractUserRequest {

    @NotNull private Integer index;

    @NotNull private Status status;

    public TaskChangeStatusByIndexRequest(@NotNull final Integer index, @NotNull final Status status) {
        this.index = index;
        this.status = status;
    }

}
