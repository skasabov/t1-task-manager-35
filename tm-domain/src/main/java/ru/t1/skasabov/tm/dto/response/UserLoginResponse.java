package ru.t1.skasabov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class UserLoginResponse extends AbstractResponse {

    @Nullable private String token;

    public UserLoginResponse(@Nullable final String token) {
        this.token = token;
    }

}
